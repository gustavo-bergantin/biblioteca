<?php
use Migrations\AbstractMigration;

class UsersBooks extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * https://book.cakephp.org/phinx/0/en/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users_books');
        $table->addColumn('user_id', 'integer')
            ->addForeignKey('user_id', 'users', 'id');

        $table->addColumn('book_id', 'integer')
            ->addForeignKey('book_id', 'books', 'id');

        $table->create();

    }
}
