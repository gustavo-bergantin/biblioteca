<?php
namespace App\Controller;

use App\Controller\UsersController;

/**
 * UsersBooks Controller
 *
 * @property \App\Model\Table\UsersBooksTable $UsersBooks
 *
 * @method \App\Model\Entity\UsersBook[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */

class ApiController extends UsersController
{

    public function add()
    {
        $this->layout = false;
        $this->loadModel('Users');
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $resultJ = json_encode($user);
                $this->response->type('json');
                $this->response->body($resultJ);
                return $this->response;
            }
            echo json_encode('Erro, verifique os dados.');
        }
        $this->set(compact('user'));

    }
}
