<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * UsersBooks Controller
 *
 * @property \App\Model\Table\UsersBooksTable $UsersBooks
 *
 * @method \App\Model\Entity\UsersBook[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersBooksController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Books'],
        ];
        $usersBooks = $this->paginate($this->UsersBooks);

        $this->set(compact('usersBooks'));
    }

    /**
     * View method
     *
     * @param string|null $id Users Book id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $usersBook = $this->UsersBooks->get($id, [
            'contain' => ['Users', 'Books'],
        ]);

        $this->set('usersBook', $usersBook);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $usersBook = $this->UsersBooks->newEntity();
        if ($this->request->is('post')) {
            $usersBook = $this->UsersBooks->patchEntity($usersBook, $this->request->getData());
            if ($this->UsersBooks->save($usersBook)) {
                $this->Flash->success(__('The users book has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The users book could not be saved. Please, try again.'));
        }
        $users = $this->UsersBooks->Users->find('list', ['limit' => 200]);
        $books = $this->UsersBooks->Books->find('list', ['limit' => 200]);
        $this->set(compact('usersBook', 'users', 'books'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Users Book id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $usersBook = $this->UsersBooks->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $usersBook = $this->UsersBooks->patchEntity($usersBook, $this->request->getData());
            if ($this->UsersBooks->save($usersBook)) {
                $this->Flash->success(__('The users book has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The users book could not be saved. Please, try again.'));
        }
        $users = $this->UsersBooks->Users->find('list', ['limit' => 200]);
        $books = $this->UsersBooks->Books->find('list', ['limit' => 200]);
        $this->set(compact('usersBook', 'users', 'books'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Users Book id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $usersBook = $this->UsersBooks->get($id);
        if ($this->UsersBooks->delete($usersBook)) {
            $this->Flash->success(__('The users book has been deleted.'));
        } else {
            $this->Flash->error(__('The users book could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
