<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UsersBook[]|\Cake\Collection\CollectionInterface $usersBooks
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?=__('Actions')?></li>
        <li><?=$this->Html->link(__('Adicionar Usuario ao Livro'), ['action' => 'add'])?></li>
        <li><?=$this->Html->link(__('Listar Usuarios'), ['controller' => 'Users', 'action' => 'index'])?></li>
        <li><?=$this->Html->link(__('Cadastrar Usuarios'), ['controller' => 'Users', 'action' => 'add'])?></li>
        <li><?=$this->Html->link(__('Listar Livros'), ['controller' => 'Books', 'action' => 'index'])?></li>
        <li><?=$this->Html->link(__('Cadastrar Livro'), ['controller' => 'Books', 'action' => 'add'])?></li>
    </ul>
</nav>
<div class="usersBooks index large-9 medium-8 columns content">
    <h3><?=__('Usuarios com Livros')?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?=$this->Paginator->sort('id')?></th>
                <th scope="col"><?=$this->Paginator->sort('user_id')?></th>
                <th scope="col"><?=$this->Paginator->sort('book_id')?></th>
                <th scope="col" class="actions"><?=__('Ações')?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($usersBooks as $usersBook): ?>
            <tr>
                <td><?=$this->Number->format($usersBook->id)?></td>
                <td><?=$usersBook->has('user') ? $this->Html->link($usersBook->user->name, ['controller' => 'Users', 'action' => 'view', $usersBook->user->id]) : ''?></td>
                <td><?=$usersBook->has('book') ? $this->Html->link($usersBook->book->name, ['controller' => 'Books', 'action' => 'view', $usersBook->book->id]) : ''?></td>
                <td class="actions">
                    <?=$this->Html->link(__('Editar'), ['action' => 'edit', $usersBook->id])?>
                    <span> / </span>
                    <?=$this->Form->postLink(__('Desvincular'), ['action' => 'delete', $usersBook->id], ['confirm' => __('Deseja desvincular o id {0} a esse livro?', $usersBook->id)])?>
                </td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?=$this->Paginator->first('<< ' . __('Primeiro'))?>
            <?=$this->Paginator->prev('< ' . __('Anterior'))?>
            <?=$this->Paginator->numbers()?>
            <?=$this->Paginator->next(__('Próximo') . ' >')?>
            <?=$this->Paginator->last(__('Ultimo') . ' >>')?>
        </ul>
        <p><?=$this->Paginator->counter(['format' => __('Pagina {{page}} de {{pages}}, mostrando {{current}} dado(s) de {{count}} registros')])?></p>
    </div>
</div>
