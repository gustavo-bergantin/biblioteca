<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UsersBook $usersBook
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?=__('Actions')?></li>
        <li><?=$this->Form->postLink(
    __('Desvincular'),
    ['action' => 'delete', $usersBook->id],
    ['confirm' => __('Você tem certeza que quer desvincular o id {0}?', $usersBook->id)]
)
?></li>
        <li><?=$this->Html->link(__('Adicionar Usuario ao Livro'), ['action' => 'add'])?></li>
        <li><?=$this->Html->link(__('Listar Usuarios'), ['controller' => 'Users', 'action' => 'index'])?></li>
        <li><?=$this->Html->link(__('Cadastrar Usuarios'), ['controller' => 'Users', 'action' => 'add'])?></li>
        <li><?=$this->Html->link(__('Listar Livros'), ['controller' => 'Books', 'action' => 'index'])?></li>
        <li><?=$this->Html->link(__('Cadastrar Livro'), ['controller' => 'Books', 'action' => 'add'])?></li>
    </ul>
</nav>
<div class="usersBooks form large-9 medium-8 columns content">
    <?=$this->Form->create($usersBook)?>
    <fieldset>
        <legend><?=__('Editar Usuario e o Vinculo do Livro')?></legend>
        <?php
echo $this->Form->control('user_id', ['options' => $users]);
echo $this->Form->control('book_id', ['options' => $books]);
?>
    </fieldset>
    <?=$this->Form->button(__('Submit'))?>
    <?=$this->Form->end()?>
</div>
