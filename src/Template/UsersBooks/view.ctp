<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\UsersBook $usersBook
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Users Book'), ['action' => 'edit', $usersBook->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Users Book'), ['action' => 'delete', $usersBook->id], ['confirm' => __('Are you sure you want to delete # {0}?', $usersBook->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Users Books'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Users Book'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Books'), ['controller' => 'Books', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Book'), ['controller' => 'Books', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="usersBooks view large-9 medium-8 columns content">
    <h3><?= h($usersBook->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $usersBook->has('user') ? $this->Html->link($usersBook->user->name, ['controller' => 'Users', 'action' => 'view', $usersBook->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Book') ?></th>
            <td><?= $usersBook->has('book') ? $this->Html->link($usersBook->book->name, ['controller' => 'Books', 'action' => 'view', $usersBook->book->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($usersBook->id) ?></td>
        </tr>
    </table>
</div>
