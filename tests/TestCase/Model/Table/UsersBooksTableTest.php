<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersBooksTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersBooksTable Test Case
 */
class UsersBooksTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\UsersBooksTable
     */
    public $UsersBooks;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.UsersBooks',
        'app.Users',
        'app.Books',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('UsersBooks') ? [] : ['className' => UsersBooksTable::class];
        $this->UsersBooks = TableRegistry::getTableLocator()->get('UsersBooks', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->UsersBooks);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
